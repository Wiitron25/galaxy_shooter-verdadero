﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoNave : MonoBehaviour
{
    public float speed; 
    //public gameObject Disparo;   

    // Update is called once per frame
    void Update()
    {
        while(true){
        transform.Translate(Vector3.right * -speed*Time.deltaTime);
        StartCoroutine(Enemyship());
        }
        

    }

     public void OnTriggerEnter2D(Collider2D other) {

         if(other.tag=="Finish"){
             Destroy(gameObject);
         }
        
    }
    IEnumerator Enemyship(){
            while(true){
            yield return new WaitForSeconds(1f);
            transform.position =new Vector3(transform.position.x,transform.position.y,0);
            }
        }
}
