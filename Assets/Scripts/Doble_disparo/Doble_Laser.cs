﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doble_Laser : MonoBehaviour
{

    // Start is called before the first frame update
   public float Speed = 10f;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * Speed * Time.deltaTime);
        if (transform.position.x>10f){
            Destroy(this.gameObject);
        }
    }

}