﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class TorcidWeapon : Weapon
{
 
    public GameObject TorcidBullet;
   
 
 
    public float cadencia;
 
    public override float GetCadencia()
    {
        return cadencia;
    }
 
    public override void Shoot()
    {
        Instantiate (TorcidBullet, this.transform.position, Quaternion.identity, null);
       
    }
}