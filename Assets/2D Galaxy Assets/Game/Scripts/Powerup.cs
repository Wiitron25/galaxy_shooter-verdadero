﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    public float Speed = 2F;
    

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down*Speed*Time.deltaTime);
    }

    private void OnTriggerEnter2D (Collider2D other){
        Debug.Log("Se ha colisionado con :" + other.name);
      if (other.tag=="Player"){
      Player player = other.GetComponent<Player>();
      if (player != null){
      player.canTripleShot = true;
      }
      Destroy(this.gameObject);
      }
    }




}
